DESTDIR =

all:

install:

clean:
	find . -name "*~" | xargs rm -f
	find . -name "__pycache__" | xargs rm -rf

.PHONY: all clean install
