/**
 * bascule pour créer ou supprimer un lien symbolique
 * @param cle le mot de passe à activer/inactiver
 * @param create vrai si on veut créer sinon faux.
 * @param csrf code de vérification de session
 **/
function lien_symbolique(cle, create, csrf){
    $.post(
	"/lien_symbolique",
	{
	    cle: cle,
	    create: create,
	    csrfmiddlewaretoken: csrf,
	}
    ).done(
	function (){
	    location.reload();
	}
    ).fail(
	function(){
	    $("#erreur").dialog();
	}
    );
}


/**
 * Création d'un nouveau mot de passe
 * @param csrf code de vérification de session
 **/
function nouvellecle(csrf){
    var cle = $("#newpass").val();
    if (cle.length < 5){
	$("#longueur").dialog();
    } else {
	$.post(
	    "/nouvelle_cle",
	    {
		cle: cle,
		csrfmiddlewaretoken: csrf,
	    }
	).done(
	    function(){
		location.reload();
	    }
	).fail(
	    function(){
		$("#erreur").dialog();
	    }
	)
    }
}

/**
 * Suppression d'un nouveau mot de passe
 * @param cle le mot de passe à activer/inactiver
 * @param csrf code de vérification de session
 **/
function supprime_cle(cle, csrf){
    $.post(
	"/supprime_cle",
	{
	    cle: cle,
	    csrfmiddlewaretoken: csrf,
	}
    ).done(
	function(){
	    location.reload();
	}
    ).fail(
	function(){
	    $("#erreur").dialog();
	}
    );
}
