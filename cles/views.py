from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from chorale.settings import BASE_DIR

import os, datetime, subprocess, uuid

from .models import Cle

def cles_list(request):
    if request.user.is_authenticated :
        cles = Cle.objects.all()
        return render(request, 'cles/cles_list.html', {
            "cles": cles,
            "random": uuid.uuid1().hex,
        })
    else:
        return redirect("/admin/login/?next=/")

def lien_symbolique(request):
    create = request.POST["create"] == 'true'
    cle = Cle.objects.filter(id = request.POST["cle"])[0]
    linkname = os.path.join("var", "www", "html", "chorale", cle.hachage)
    if create:
        cmd = ["ln", "-s", "Aap5Ac7E", linkname]
        subprocess.run(cmd)
        cle.fin = None
        cle.save()
    else:
        cmd = ["rm", "-f", linkname]
        subprocess.run(cmd)
        cle.fin = timezone.now() - datetime.timedelta(seconds=1)
        cle.save()
    return HttpResponse("ok", content_type='text/plain')


def nouvelle_cle(request):
       cle = Cle(cle = request.POST["cle"])
       linkname = os.path.join("var", "www", "html", "chorale", cle.hachage)
       cmd = ["ln", "-s", "Aap5Ac7E", linkname]
       subprocess.run(cmd)
       cle.save()
       return HttpResponse("ok", content_type='text/plain')
       
def supprime_cle(request):
    cle = Cle.objects.filter(id = request.POST["cle"])[0]
    linkname = os.path.join("var", "www", "html", "chorale", cle.hachage)
    cmd = ["rm", "-f", linkname]
    subprocess.run(cmd)
    cle.delete()
    return HttpResponse("ok", content_type='text/plain')
       
