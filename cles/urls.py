from django.urls import path
from . import views
urlpatterns = [
    path('lien_symbolique', views.lien_symbolique, name='lien_symbolique'),
    path('nouvelle_cle', views.nouvelle_cle, name='nouvelle_cle'),
    path('supprime_cle', views.supprime_cle, name='supprime_cle'),
    path('', views.cles_list, name='cles_list'),
]
