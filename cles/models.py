from django.db import models
from django.utils import timezone
from hashlib import sha256

class Cle(models.Model):
    cle   = models.CharField(max_length=20)
    debut = models.DateTimeField(default=timezone.now)
    fin   = models.DateTimeField(blank=True, null=True)

    @property
    def active(self):
        t = timezone.now()
        return t > self.debut and (self.fin == None or t <= self.fin)

    @property
    def finale(self):
        if self.fin:
            return self.fin
        else:
            return '∞'

    @property
    def hachage(self):
        return sha256(str(self.cle).encode('utf-8')).hexdigest()

    def __str__(self):
        return f"{self.cle} ({'active' if self.active else 'inactive'}) : {self.debut} ➜ {self.finale}"

